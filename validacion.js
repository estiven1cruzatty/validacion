    document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("formulario_Vehiculos").addEventListener('submit', validarCampo); 
    });
    
    function validarCampo(M) {
    M.preventDefault();
    var codigo = document.getElementById('codigo').value;
    var Exprecion_alfanumerica = /^[0-9a-zA-z]{1}+&/
    if(codigo.length != 5) {
      alert('El codigo debe tener 5 caracteres');
      return;
    }
    else{
      if(!codigo.match(Exprecion_alfanumerica)){
         return alert("Introducir al menos una letra y un numero");
      }
    }
    var Marca = document.getElementById('Marca').value;
    if(Marca.length != 50) {
      alert('la marca del vehiculo debe tener maximo 50 caracteres');
      return;
    }
    else{
      if(!Marca.match(Exprecion_alfanumerica)){
          return alert("Ingresa al menos 5 caracteres");
      }
    }
    
    var Modelo = document.getElementById('Modelo').value;
    if(Modelo.length != 30) {
      alert('El modelo del vehiculo debe tener maximo 30 caracteres');
      return;
    }
    else{
      if(!Modelo.match(Exprecion_alfanumerica)){
          return alert("Ingresa al menos 5 caracteres");
      }
    }
    
    var año = document.getElementById('año').value;
    var Exprecion_Numerica =/^\d{1,4}$/
    if(año.length != 4) {
      alert('El año debe tener 4 dígitos');
      return;
    }
    else{
      if(!año.match(Exprecion_Numerica)){
          return alert("No puede quedar vacio");
      }
    }
    
    var fecha_inicio = document.getElementById('fecha_inicio').value;
    
    var Expre_tipofecha = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
    
    
    if ((fecha_inicio.match(Expre_tipofecha)) && (fecha_inicio!='')) {
              return true;
      } else {
          alert("Digita una fecha valida")
      }
    var fecha_final = document.getElementById('fecha_final').value;
    
    if ((fecha_final.match(Expre_tipofecha)) && (fecha_final!='')) {
    return true;
    } else {
        alert("Digita una fecha valida")
    }
    
    
    if(fecha_final > fecha_inicio) {
      alert('Fecha de final debe ser mayor a fecha de inicio');
      return;
    }
    
    this.submit();
    }